Feature('Main page of tender.pro').tag('main');


Scenario('Redirect from tender.pro to new.tender.pro', (I) => {
  I.say('Go to old address');
  I.amOnPage('tender.pro');
  I.wait(3);
  I.say('Redirect is working');
  I.seeInCurrentUrl("https://new.tender.pro/");
});



Scenario('Search on the main page', (I) => {
  I.say('Main page is available');
  I.amOnPage('/');
  I.seeElement({css: "svg[data-name='logo']"});

  I.say('Search input is available');
  I.seeElement({css: 'input[placeholder="Найти товар"]'});

  I.say('Suggested links are working');
  I.click(locate('div')
  	.withAttr({ class: 'suggest--nav'} )
  	.find(locate('span')
  		.withText('Компании')));

  I.wait(1);
  I.dontSeeElement({css: 'input[placeholder="Найти товар"]'});
  I.seeElement({css: 'input[placeholder="Найти компанию"]'});

  I.say('Company search request is working');
  I.fillField({css: 'input[placeholder="Найти компанию"]'}, 'Руссдрагмет');
  I.click(locate('span').withAttr({ class: 'icon search' }));
  I.wait(1);

  I.seeInCurrentUrl('https://new.tender.pro/search?_type=company&q=Руссдрагмет');

	I.seeElement(locate('li')
		.withAttr({ class: 'iac--tabs_item iac--tabs_item--active' })
		.find(locate('span')
			 .withAttr({ class: 'item' })
			 .withText('Компании')));

});