var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

// request parameters
const parameters_arr = [
{key: '1732ede4de680a0c93d81f01d7bac7d1',
set_type_id: '3',
max_rows: '3',
open_only: 't',
set_id: '29568'},
{key: '',
set_type_id: '1',
max_rows: '3',
open_only: 't',
set_id: '29568'},
{key: '1732ede4de680a0c93d81f01d7bac7d1',
set_type_id: '3',
max_rows: '3',
open_only: 't',
set_id: '29560008'}
]


Feature('GET api request').tag('api');

Scenario('positive test', async (I) => {

    I.say("Ответ должен выдавать список тендеров для компании с ид=29568, только открытые, максимум 3");
	let params = parameters_arr[0];

	let  req =
	  await I.sendGetRequest(`info.tenderlist_by_set.json?_key=${params.key}&set_type_id=${params.set_type_id}&set_id=${params.set_id}&max_rows=${params.max_rows}&open_only=${params.open_only}`);

	I.say("Проверяем статус запроса: код =200 и success=true");
    expect(req.status).to.equal(200);
    expect(req.data["success"]).to.equal("true");

    I.say("Проверяем, что статус тендеров в ответе только открытый");
    for (let i of req.data["result"]["data"]) {
    	    expect(i["status_id"]).to.equal(2);
    }

    I.say("Проверяем, что ответ содержит все необходимые ключи");
    expect(req.data["result"]["args"]).to.have.all.keys("_key", "types", "max_rows", "open_only", "set_id", "set_type_id", "offset");
    expect(req.data["result"]["data"][0]).to.have.all.keys(  "is_delivery_required",
            "company_rating",
            "company_name",
            "delivery_area_id",
            "is_223fz",
            "close_date",
            "type_id",
            "ship_date",
            "open_date",
            "id",
            "status_id",
            "type_name",
            "dinamic_fields",
            "delivery_address",
            "currency_id",
            "company_id",
            "is_priv",
            "is_hidden",
            "delivery_country_id",
            "delivery_region_id",
            "possibleclose_date",
            "finish_date",
            "currency_name",
            "title");


        I.say("Проверяем, что в ответе содержится не более 3 тендеров");
        expect(req.data["result"]["data"]).to.have.lengthOf.at.most(3);
});


Scenario('_key is empty', async (I) => {

    I.say("Ответ с пустым ключом должне выдавать succes=false и result.error с id=_key и сообщением о том, что не задано обязательно значение");
	let params = parameters_arr[1];
	let  req =  await I.sendGetRequest(`info.tenderlist_by_set.json?_key=${params.key}&set_type_id=${params.set_type_id}&set_id=${params.set_id}&max_rows=${params.max_rows}&open_only=${params.open_only}`);

	I.say("Проверяем статус запроса: код =200 и success=false");
    expect(req.status).to.equal(200);
    expect(req.data["success"]).to.equal("false");

    I.say("Проверяем, что ответ содержит все необходимые ключи");
    expect(req.data["result"]["args"]).to.have.all.keys("types", "max_rows", "open_only", "set_id", "set_type_id", "offset");
    expect(req.data["result"]["args"]).to.not.have.any.keys("_key");

    I.say("Проверяем данные в ответе");
    expect(req.data["result"]["error"][0]["id"]).to.equal("_key");
    expect(req.data["result"]["error"][0]["message"]).to.equal("не задано обязательное значение");
    expect(req.data["result"]["error"][0]["code"]).to.equal("Y0001");

});


Scenario('Company does not exist', async (I) => {

    I.say("Ответ с несуществующей компанией должен выдавать пустой массив data");
	let params = parameters_arr[2];
	let  req =  await I.sendGetRequest(`info.tenderlist_by_set.json?_key=${params.key}&set_type_id=${params.set_type_id}&set_id=${params.set_id}&max_rows=${params.max_rows}&open_only=${params.open_only}`);

	I.say("Проверяем статус запроса: код =200 и success=true");
    expect(req.status).to.equal(200);
    expect(req.data["success"]).to.equal("true");

    I.say("Проверяем, что ответ содержит все необходимые ключи");
    expect(req.data["result"]["args"]).to.have.all.keys("_key", "types", "max_rows", "open_only", "set_id", "set_type_id", "offset");
    
    I.say("Проверяем, что массив с данными пуст");
    expect(req.data["result"]["data"]).to.be.empty;
});

