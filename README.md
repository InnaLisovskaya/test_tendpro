# test_tendpro

Чтобы запустить тесты, вам нужно 


1. создать у себя директорию, 

mkdir tests

cd tests

2. инициировать гит

git init

3. склонировать репозиторий и перейти в директорию тестов

git clone git@gitlab.com:InnaLisovskaya/test_tendpro.git

cd test_tendpro

4. установить зависимости

npm install  

5. запустить тесты  

npx codeceptjs run --debug



Все тесты находятся в директории test_tenderpro/tests.

