exports.config = {
  tests: './tests/*_test.js',
  output: './output',
  helpers: {
    WebDriver: {
      url: 'https://new.tender.pro/',
      browser: 'chrome',
      desiredCapabilities: {
        chromeOptions: {
          args: [ '--headless','--disable-gpu', '--window-size=2048,1080'],
       //     args: ['--disable-gpu', '--window-size=1280,1200'],
        },
      },
    },
    REST: {
      endpoint: 'http://www.tender.pro/api/_',
    },
  },
  include: {
    I: './steps_file.js'
  },
  plugins: {
    wdio: {
      enabled: true,
      services: ['selenium-standalone']
    }
  },

  bootstrap: null,
  mocha: {},
  name: 'test_tendpro'
}